defmodule TimeManagerApi.Teams do
  @moduledoc """
  The Teams context.
  """

  import Ecto.Query, warn: false
  alias TimeManagerApi.Repo
  alias TimeManagerApi.Teams.Team
  alias TimeManagerApi.Accounts.User
  alias TimeManagerApi.TeamUser
  alias TimeManagerApi.Guardian

  @doc """
  Returns the list of teams.

  ## Examples

      iex> list_teams()
      [%Team{}, ...]

  """
  def list_teams(id) do
    user = Repo.get(User, id)
    teams = Repo.all(Ecto.assoc(user, :teams))
    Repo.preload(teams, [{:users, :role}, :manager])
  end

  def list_teams_of_manager(id) do
    # find teams of manager
    teams = Repo.all(from t in Team, where: t.manager_id == ^id)
    Repo.preload(teams, [{:users, :role}, :manager])
  end

  def add_user(conn, id, userID) do
    team = Repo.get(Team, id)
    user = Guardian.Plug.current_resource(conn)

    if user.id == team.manager_id do
      changeset = TeamUser.changeset(%TeamUser{}, %{user_id: userID, team_id: id})
      Repo.insert(changeset)
    else
      {:error, :unauthorized}
    end
  end

  def remove_user(conn, id, userID) do
    team = Repo.get(Team, id)
    user = Guardian.Plug.current_resource(conn)

    if user.id == team.manager_id do
      teamUser = Repo.get_by(TeamUser, [team_id: id, user_id: userID])
      Repo.delete(teamUser)
    else
      {:error, :unauthorized}
    end
  end


  @doc """
  Gets a single team.

  Raises `Ecto.NoResultsError` if the Team does not exist.

  ## Examples

      iex> get_team!(123)
      %Team{}

      iex> get_team!(456)
      ** (Ecto.NoResultsError)

  """
  def get_team!(id), do: Repo.get!(Team, id)

  @doc """
  Creates a team.

  ## Examples

      iex> create_team(%{field: value})
      {:ok, %Team{}}

      iex> create_team(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_team(attrs \\ %{}) do
    %Team{}
    |> Team.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a team.

  ## Examples

      iex> update_team(team, %{field: new_value})
      {:ok, %Team{}}

      iex> update_team(team, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_team(%Team{} = team, attrs) do
    team
    |> Team.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Team.

  ## Examples

      iex> delete_team(team)
      {:ok, %Team{}}

      iex> delete_team(team)
      {:error, %Ecto.Changeset{}}

  """
  def delete_team(%Team{} = team) do
    Repo.delete(team)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking team changes.

  ## Examples

      iex> change_team(team)
      %Ecto.Changeset{source: %Team{}}

  """
  def change_team(%Team{} = team) do
    Team.changeset(team, %{})
  end
end
