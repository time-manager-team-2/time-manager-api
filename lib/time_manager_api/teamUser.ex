defmodule TimeManagerApi.TeamUser do
    use Ecto.Schema
  
    @primary_key false
    schema "teams_users" do
      belongs_to :team, TimeManagerApi.Teams.Team, primary_key: true
      belongs_to :user, TimeManagerApi.Users.User, primary_key: true
    end
  
    def changeset(struct, params \\ %{}) do
      struct
      |> Ecto.Changeset.cast(params, [:team_id, :user_id])
      |> Ecto.Changeset.validate_required([:team_id, :user_id])
      # Maybe do some counter caching here!
    end
  end