defmodule TimeManagerApi.Clocks do
  @moduledoc """
  The Clocks context.
  """

  import Ecto.Query, warn: false
  alias TimeManagerApi.Repo

  alias TimeManagerApi.Clocks.Clock
  alias TimeManagerApi.Workingtimes.Workingtime
  alias TimeManagerApi.Guardian

  @doc """
  Gets a single clock.

  Raises `Ecto.NoResultsError` if the Clock does not exist.

  ## Examples

      iex> get_clock!(123)
      %Clock{}

      iex> get_clock!(456)
      ** (Ecto.NoResultsError)

  """
  def get_clock_by_user!(conn, id) do
    user = Guardian.Plug.current_resource(conn)

    if user.id == String.to_integer(id) do
      Repo.get_by(Clock, [user_id: id])
    else
      {:error, :unauthorized}
    end
  end

  @doc """
  Creates a clock.

  ## Examples

      iex> create_clock(%{field: value})
      {:ok, %Clock{}}

      iex> create_clock(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_or_update_clock(conn, id) do
    user = Guardian.Plug.current_resource(conn)

    if user.id == String.to_integer(id) do
      clock = Repo.get_by(Clock, [user_id: id])
      if clock do
        update_clock(clock)
      else
        create_clock(id)
      end
    else
      {:error, :unauthorized}
    end
  end

  defp create_clock(id) do
    # get current date
    {:ok, datetime} = DateTime.now("Europe/Paris")

    %Clock{}
    |> Clock.changeset(%{user_id: String.to_integer(id), time: datetime, status: true})
    |> Repo.insert()
  end

  defp update_clock(clock) do
    # get current date
    {:ok, datetime} = DateTime.now("Europe/Paris")

    isDeparture = clock.status
    startTime = clock.time

    # Departure -> insert a working time
    if isDeparture do
      attrs = Workingtime.changeset(%Workingtime{},
      %{"user_id" => clock.user_id, "start" => startTime, "end" => datetime})

      Repo.insert(attrs)
    end

    attrs = if !isDeparture do
      # Arrival of the user (update departure date)
      Clock.changeset(clock, %{"status" => "true", "time" => datetime})
    else
      # departure of the user
      Clock.changeset(clock, %{"status" => "false"})
    end

    # update clock
    Repo.update(attrs)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking clock changes.

  ## Examples

      iex> change_clock(clock)
      %Ecto.Changeset{source: %Clock{}}

  """
  def change_clock(%Clock{} = clock) do
    Clock.changeset(clock, %{})
  end
end
