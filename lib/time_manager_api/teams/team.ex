defmodule TimeManagerApi.Teams.Team do
  use Ecto.Schema
  import Ecto.Changeset
  alias TimeManagerApi.Repo
  alias TimeManagerApi.TeamUser
  alias TimeManagerApi.Accounts.User

  schema "teams" do
    field :title, :string
    many_to_many :users, TimeManagerApi.Accounts.User, join_through: TeamUser #, on_replace: :delete
    belongs_to :manager, TimeManagerApi.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(team, attrs) do
    team
    |> cast(attrs, [:title, :manager_id])
    |> validate_required([:title, :manager_id])
    |> validate_manager(:manager_id)
  end

  # verify manager_id is a manager
  defp validate_manager(changeset, field, options \\ []) do
    validate_change(changeset, field, fn _, manager_id ->
      user = Repo.get(User, changeset.changes[:manager_id])
      
      case user.role_id >= 2 do
        true -> []
        false -> [{field, options[:message] || "Unexpected manager id"}]
      end
    end)
  end
end
