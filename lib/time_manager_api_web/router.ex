defmodule TimeManagerApiWeb.Router do
  use TimeManagerApiWeb, :router

  alias TimeManagerApi.Guardian

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :jwt_authenticated do
    plug Guardian.AuthPipeline
  end

  scope "/", TimeManagerApiWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # need to be authenticated
  scope "/api", TimeManagerApiWeb do
    pipe_through [:api, :jwt_authenticated]

    scope "/users" do
      # user crud
      get "/", UserController, :index
      get "/my_user", UserController, :show
      delete "/:userID", UserController, :delete
      # e mail
      put "/changeMail", UserController, :changeMail
      # Roles
      put "/:userID/changeRole", UserController, :changeRole
      put "/:userID", UserController, :update
      # sign out
      post "/sign_out", UserController, :sign_out
    end

    scope "/clocks" do
      # clock
      get "/:userID", ClockController, :show
      post "/:userID", ClockController, :create
    end

    scope "/workingtimes" do
      # working times
      get "/:userID", WorkingtimeController, :index
      # get WorkingTimes of team
      get "/team/:teamID", WorkingtimeController, :teamWorkingTimes
      get "/:userID/:workingtimeID", WorkingtimeController, :show
      post "/:userID", WorkingtimeController, :create
      put "/:id", WorkingtimeController, :update
      delete "/:id", WorkingtimeController, :delete
    end

    scope "/teams" do
      # teams
      get "/:userID", TeamController, :index
      get "/manager/:managerID", TeamController, :showManager
      post "/", TeamController, :create
      post "/manager/:teamID/user", TeamController, :addUser
      delete "/manager/:teamID/user", TeamController, :removeUser
      delete "/:teamID", TeamController, :delete
    end
  end

  scope "/api", TimeManagerApiWeb do
    pipe_through :api

    scope "/users" do
      post "/sign_up", UserController, :create
      post "/sign_in", UserController, :sign_in
    end
  end
end
