defmodule TimeManagerApiWeb.WorkingtimeController do
  use TimeManagerApiWeb, :controller

  alias TimeManagerApi.Workingtimes
  alias TimeManagerApi.Workingtimes.Workingtime
  alias TimeManagerApi.Teams.Team
  alias TimeManagerApi.Repo
  alias TimeManagerApi.Guardian

  action_fallback TimeManagerApiWeb.FallbackController

  def index(conn, %{"userID" => id}) do
    user = Guardian.Plug.current_resource(conn)

    if user.id == String.to_integer(id) or user.role_id >= 2 do
      workingtimes = Workingtimes.list_workingtimes_by_user(id)
      workingtimes = Repo.preload(workingtimes, :user)
      render(conn, "index.json", workingtimes: workingtimes)
    else
      {:error, :unauthorized}
    end
  end

  def create(conn, %{"workingtime" => workingtime_params, "userID" => id}) do
    user = Guardian.Plug.current_resource(conn)

    if user.id == String.to_integer(id) do
      with {:ok, %Workingtime{} = workingtime} <- Workingtimes.create_workingtime_for_user(workingtime_params, id) do
        workingtime = Repo.preload(workingtime, :user)
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.workingtime_path(conn, :show, id, workingtime))
        |> render("show.json", workingtime: workingtime)
      end
    else
      {:error, :unauthorized}
    end
  end

  def show(conn, %{"userID" => id, "workingtimeID" => workingtimeID}) do
    user = Guardian.Plug.current_resource(conn)

    if user.id == String.to_integer(id) do
      workingtime = Workingtimes.get_workingtime_by_user!(id, workingtimeID)
      workingtime = Repo.preload(workingtime, :user)
      render(conn, "show.json", workingtime: workingtime)
    else
      {:error, :unauthorized}
    end
  end

  def update(conn, %{"id" => id, "workingtime" => workingtime_params}) do
    user = Guardian.Plug.current_resource(conn)
    workingtime = Workingtimes.get_workingtime!(id)

    if user.id == workingtime.user_id do
      with {:ok, %Workingtime{} = workingtime} <- Workingtimes.update_workingtime(workingtime, workingtime_params) do
        workingtime = Repo.preload(workingtime, :user)
        render(conn, "show.json", workingtime: workingtime)
      end
    else
      {:error, :unauthorized}
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Guardian.Plug.current_resource(conn)
    workingtime = Workingtimes.get_workingtime!(id)

    if user.id == workingtime.user_id do
      with {:ok, %Workingtime{}} <- Workingtimes.delete_workingtime(workingtime) do
        send_resp(conn, :no_content, "")
      end
    else
      {:error, :unauthorized}
    end
  end

  def teamWorkingTimes(conn, %{"teamID" => id}) do
    user = Guardian.Plug.current_resource(conn)

    # only manager can
    if user.role_id == 2 do
      team = Repo.get(Team, String.to_integer(id))
      team = Repo.preload(team, [{:users, :role}, :manager])

      # construct list of users ids
      usersIDs = for item <- team.users do
        item.id
      end

      workingtimes = Workingtimes.list_workingtimes_all_users(usersIDs)
      workingtimes = Repo.preload(workingtimes, :user)
      render(conn, "index.json", workingtimes: workingtimes)
    else
      {:error, :unauthorized}
    end
  end
end
