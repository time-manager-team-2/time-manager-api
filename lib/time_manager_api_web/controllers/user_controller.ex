defmodule TimeManagerApiWeb.UserController do
  use TimeManagerApiWeb, :controller

  alias TimeManagerApi.Accounts
  alias TimeManagerApi.Repo
  alias TimeManagerApi.Accounts.User
  alias TimeManagerApi.Guardian

  action_fallback TimeManagerApiWeb.FallbackController

  def index(conn, _params) do
    users = Accounts.list_users()
    users = Repo.preload(users, :role)
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Accounts.create_user(user_params) do
      user = Accounts.preloadRole(user)
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_path(conn, :show))
      |> render("show.json", user: user)
    end
  end

  def sign_in(conn, %{"email" => email, "password" => password}) do
    case Accounts.token_sign_in(email, password) do
      {:ok, token, _claims} ->
        conn |> render("jwt.json", jwt: token)
      _ ->
        {:error, :unauthorized}
    end
  end

  def sign_out(conn, _params) do
    jwt = Guardian.Plug.current_token(conn)
    Guardian.revoke(jwt)

    json(conn, nil)
  end

  def show(conn, _params) do
    user = Guardian.Plug.current_resource(conn)
    user = Accounts.preloadRole(user)

    conn |> render("user.json", user: user)
  end

  def update(conn, %{"userID" => id, "user" => user_params}) do
    user = Guardian.Plug.current_resource(conn)
    # you can update only your account
    if user.id == String.to_integer(id) do
      with {:ok, %User{} = user} <- Accounts.update_user(user, user_params) do
        user = Accounts.preloadRole(user)
        render(conn, "show.json", user: user)
      end
    else
      {:error, :unauthorized}
    end
  end

  def delete(conn, %{"userID" => id}) do
    user = Guardian.Plug.current_resource(conn)
    # you can delete your account or admin can delete all
    if (user.id == String.to_integer(id) or user.role_id == 3) do
      user = Accounts.get_user!(id)
      with {:ok, %User{}} <- Accounts.delete_user(user) do
        send_resp(conn, :no_content, "")
      end
    else
      {:error, :unauthorized}
    end
  end

  def changeRole(conn, %{"userID" => id} = params) do
    with {:ok, %User{} = user} <- Accounts.change_role(conn, id, params) do
      user = Accounts.preloadRole(user)
      render(conn, "show.json", user: user)
    end
  end

  def changeMail(conn, params) do
    with {:ok, %User{} = user} <- Accounts.change_mail(conn, params) do
      user = Accounts.preloadRole(user)
      render(conn, "show.json", user: user)
    end
  end

end
