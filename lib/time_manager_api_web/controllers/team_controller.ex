defmodule TimeManagerApiWeb.TeamController do
  use TimeManagerApiWeb, :controller

  alias TimeManagerApi.Repo
  alias TimeManagerApi.Teams
  alias TimeManagerApi.Teams.Team
  alias TimeManagerApi.TeamUser
  alias TimeManagerApi.Guardian

  action_fallback TimeManagerApiWeb.FallbackController

  # Show all teams of a user
  def index(conn, %{"userID" => id}) do
    user = Guardian.Plug.current_resource(conn)

    if user.id == String.to_integer(id) or user.role_id >= 2 do
      teams = Teams.list_teams(id)
      render(conn, "index.json", teams: teams)
    else
      {:error, :unauthorized}
    end
  end

  # show all teams manage by the user (manager)
  def showManager(conn, %{"managerID" => id}) do
    user = Guardian.Plug.current_resource(conn)

    if user.id == String.to_integer(id) do
      teams = Teams.list_teams_of_manager(id)
      render(conn, "index.json", teams: teams)
    else
      {:error, :unauthorized}
    end
  end

  # create a team
  def create(conn, %{"team" => team_params}) do
    user = Guardian.Plug.current_resource(conn)

    if user.id == team_params["manager_id"] do
      with {:ok, %Team{} = team} <- Teams.create_team(team_params) do
        team = Repo.preload(team, [{:users, :role}, :manager])
        conn
        |> put_status(:created)
        |> render("show.json", team: team)
      end
    else
      {:error, :unauthorized}
    end
  end

  def delete(conn, %{"teamID" => id}) do
    team = Teams.get_team!(id)
    user = Guardian.Plug.current_resource(conn)

    if user.id == team.manager_id do
      with {:ok, %Team{}} <- Teams.delete_team(team) do
        send_resp(conn, :no_content, "")
      end
    else
      {:error, :unauthorized}
    end
  end

  # add user to a team
  def addUser(conn, %{"teamID" => id, "user_id" => userID}) do
    with {:ok, %TeamUser{} = team} <- Teams.add_user(conn, id, userID) do
      team = Repo.get(Team, id)
      team = Repo.preload(team, [{:users, :role}, :manager])
      conn
      |> put_status(:created)
      |> render("show.json", team: team)
    end
  end

  # remove user to a team
  def removeUser(conn, %{"teamID" => id, "user_id" => userID}) do
      with {:ok, %TeamUser{}} <- Teams.remove_user(conn, String.to_integer(id), userID) do
        team = Repo.get(Team, id)
        team = Repo.preload(team, [{:users, :role}, :manager])
        conn
        |> put_status(200)
        |> render("show.json", team: team)
      end
  end
end
