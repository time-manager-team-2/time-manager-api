defmodule TimeManagerApiWeb.ClockController do
  use TimeManagerApiWeb, :controller

  alias TimeManagerApi.Clocks
  alias TimeManagerApi.Clocks.Clock

  action_fallback TimeManagerApiWeb.FallbackController

  def create(conn, %{"userID" => id}) do
    with {:ok, %Clock{} = clock} <- Clocks.create_or_update_clock(conn, id) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.clock_path(conn, :show, id))
      |> render("show.json", clock: clock)
    end
  end

  def show(conn, %{"userID" => id}) do
    clock = Clocks.get_clock_by_user!(conn, id)
    render(conn, "show.json", clock: clock)
  end

  # def update(conn, %{"id" => id, "clock" => clock_params}) do
  #   clock = Clocks.get_clock!(id)

  #   with {:ok, %Clock{} = clock} <- Clocks.update_clock(clock, clock_params) do
  #     render(conn, "show.json", clock: clock)
  #   end
  # end

  # def delete(conn, %{"id" => id}) do
  #   clock = Clocks.get_clock!(id)

  #   with {:ok, %Clock{}} <- Clocks.delete_clock(clock) do
  #     send_resp(conn, :no_content, "")
  #   end
  # end
end
