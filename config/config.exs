# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :time_manager_api,
  ecto_repos: [TimeManagerApi.Repo]

# Configures the endpoint
config :time_manager_api, TimeManagerApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "tcQHuP0u/jZSTa2chZ4sxeqTV4OccQjF6FQHRWyZarnPk1GdPZ/+NZYHHyqWVuf0",
  render_errors: [view: TimeManagerApiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TimeManagerApi.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

config :time_manager_api, TimeManagerApi.Guardian,
       issuer: "time_manager_api",
       secret_key: "YwdVyknSKZQQku5CyBenSmvlvySMqps5yCU0byj4h8Lp4NUsix+byQideKSJXBjg"

config :guardian, Guardian.DB,
       repo: TimeManagerApi.Repo,
       schema_name: "guardian_tokens", # default
       sweep_interval: 60 # default: 60 minutes