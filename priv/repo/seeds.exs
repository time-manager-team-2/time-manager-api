# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     TimeManagerApi.Repo.insert!(%TimeManagerApi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

# insert default roles
roleExist = TimeManagerApi.Repo.exists?(TimeManagerApi.Roles.Role)

if !roleExist do
    TimeManagerApi.Repo.insert! %TimeManagerApi.Roles.Role{
        id: 1,
        name: "employee"
    }
    TimeManagerApi.Repo.insert! %TimeManagerApi.Roles.Role{
        id: 2,
        name: "manager"
    }
    TimeManagerApi.Repo.insert! %TimeManagerApi.Roles.Role{
        id: 3,
        name: "admin"
    }

    # use changeset to generate password hash
    changeset = TimeManagerApi.Accounts.User.changeset(%TimeManagerApi.Accounts.User{},
        %{"id" => 1, "email" => "employee@epitech.eu", "password" => "employee", "password_confirmation" => "employee", "role_id" => 1})
    user1 = TimeManagerApi.Repo.insert!(changeset)

    changeset = TimeManagerApi.Accounts.User.changeset(%TimeManagerApi.Accounts.User{},
        %{"id" => 2, "email" => "employee2@epitech.eu", "password" => "employee", "password_confirmation" => "employee", "role_id" => 1})
    user2 = TimeManagerApi.Repo.insert!(changeset)

    changeset = TimeManagerApi.Accounts.User.changeset(%TimeManagerApi.Accounts.User{},
        %{"id" => 3, "email" => "employee3@epitech.eu", "password" => "employee", "password_confirmation" => "employee", "role_id" => 1})
    user3 = TimeManagerApi.Repo.insert!(changeset)

    changeset = TimeManagerApi.Accounts.User.changeset(%TimeManagerApi.Accounts.User{},
        %{"id" => 4, "email" => "manager@epitech.eu", "password" => "managermanager", "password_confirmation" => "managermanager", "role_id" => 2})
    manager = TimeManagerApi.Repo.insert!(changeset)

    changeset = TimeManagerApi.Accounts.User.changeset(%TimeManagerApi.Accounts.User{},
        %{"id" => 5, "email" => "admin@epitech.eu", "password" => "adminadmin", "password_confirmation" => "adminadmin", "role_id" => 3})
    TimeManagerApi.Repo.insert!(changeset)

    # insert default teams
    team = TimeManagerApi.Repo.insert! %TimeManagerApi.Teams.Team{
        id: 1,
        title: "Team 1",
        manager_id: manager.id
    }
    team2 = TimeManagerApi.Repo.insert! %TimeManagerApi.Teams.Team{
        id: 2,
        title: "Team 2",
        manager_id: manager.id
    }

    # insert user in the team (many to many)
    TimeManagerApi.Repo.insert! %TimeManagerApi.TeamUser{
        team_id: team.id,
        user_id: user1.id
    }
    TimeManagerApi.Repo.insert! %TimeManagerApi.TeamUser{
        team_id: team2.id,
        user_id: user2.id
    }
    TimeManagerApi.Repo.insert! %TimeManagerApi.TeamUser{
        team_id: team2.id,
        user_id: user3.id
    }
end