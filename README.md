# Time Manager Api

## Run project

You need to have install elixr and Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

## Docker setup

see project CI [here](https://gitlab.com/time-manager-team-2/time-manager-ci) for more informations


### Created by

 * Sylvain Dupuy
 * Michaël Lucas 
 * David Schoffit
 * Said Mammar