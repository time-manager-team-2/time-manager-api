FROM elixir:otp-22

COPY ./entrypoint.sh /

COPY  . .

RUN mix local.hex --force \
    && mix local.rebar --force \
    && mix deps.get

EXPOSE 4000

RUN chmod +x /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]